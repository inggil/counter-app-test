export class User {
  userid: number;
  username: string;

  constructor(data?: any) {
    Object.assign(this, data);
  }
}
