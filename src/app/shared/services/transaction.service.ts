
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_HTTP } from '.';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient) {


  }
  getOperator() {
    return this.http.get(`${BASE_HTTP}operator`);
  }
  getVoucer(params) {
    return this.http.get(`${BASE_HTTP}voucher`, {params: params});
  }
  doTransaction(params) {
    return this.http.get(`${BASE_HTTP}transaction`, {params: params});
  }
}
