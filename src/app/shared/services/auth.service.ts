
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_HTTP } from '.';
import { User } from '../models/user';

@Injectable()
export class AuthService {
  user: User;
  constructor(private http: HttpClient) {


  }
  login(params) {
    return this.http.get(`${BASE_HTTP}login`, {params: params});
  }
  setUser(userData: User) {
    this.user = userData;
    localStorage.setItem('user', JSON.stringify(userData));
  }
  getUser() {
    const userData = localStorage.getItem('user');
    this.user = JSON.parse(userData);
    return this.user;
  }
}
