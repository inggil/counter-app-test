import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
  succes(text: string) {
      this.show(text, { classname: 'bg-success text-light toast-costum'});
  }
  warning(text: string) {
      this.show(text, { classname: 'bg-warning text-light toast-costum'});
  }
  danger(text: string) {
      this.show(text, { classname: 'bg-danger text-light toast-costum'});
  }
}
