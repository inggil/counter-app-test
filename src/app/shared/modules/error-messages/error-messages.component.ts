import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'error-messages',
  template: `
    <div [hidden]="!errors.length" class="error-message" [ngClass]="{'left':left}" ><strong>{{errorText}}</strong></div>
  `
})
export class ErrorMessagesComponent implements OnChanges {
  @Input('data') data: any = {};
  @Input('left') left = false;

  errorText: string;
  errors: Array<string> = [];

  ngOnChanges(input: any) {
    this.intError();
  }
  intError() {
    this.errors = [];
    if (this.data && this.data.required) { this.errors.push('required'); }
    if (this.data && this.data.maxlength) { this.errors.push('to long'); }
    if (this.data && this.data.minlength) { this.errors.push('to sort'); }
    if (this.data && this.data.pattern) { this.errors.push('pattern error'); }
    if (this.data && this.data.equal) { this.errors.push('not equal'); }
    this.errorText = this.errors.join(', ');
  }
}
// use
// <error-messages [data]="loginForm.controls?.email?.errors"></error-messages>
