import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { ErrorMessageModule } from '../shared/modules/error-messages/error-messages.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ErrorMessageModule,
        LoginRoutingModule],
    declarations: [LoginComponent]
})
export class LoginModule {}
