

import { AuthService } from './../shared/services/auth.service';
import { ToastService } from '../shared/services/toast.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { NgForm } from '@angular/forms';
import { User } from '../shared/models/user';
import { toastProperty } from '../shared';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    user: User;
    constructor(
      public router: Router,
      public auth: AuthService,
      public toastService: ToastService,

    ) {}
    ngOnInit() {
    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }
    doLogin(form: NgForm) {
        if (form.invalid) {
            this.toastService.warning('Please Check Your Form !');
        }
        if (form.valid) {
            this.auth.login(form.value).toPromise().then((res: any) => {
                this.toastService[toastProperty[res.status]](res.msg);
                if (Number(res.status)) {
                    this.user = new User();
                    this.user.userid = res.userid;
                    this.user.username = form.value.username;
                    this.auth.setUser(this.user);
                    this.router.navigate(['transaction'], { replaceUrl: true });
                }
            });
        }
    }
}
