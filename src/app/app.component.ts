
import { ToastService } from './shared/services/toast.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { NavigationStart, NavigationEnd, NavigationCancel, NavigationError, Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    sub: any;
    loading = false;
    constructor(
        public router: Router,
        public toastService: ToastService,
    ) {
    }

    ngOnInit() {
        this.setRouterListener();

    }

    setRouterListener() {
        this.sub = this
            .router
            .events
            .subscribe(event => {
            if (event instanceof NavigationStart) { }
            if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) { }
        }, (error: any) => { });
    }

    isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }
}
