import { ToastService } from './../../shared/services/toast.service';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { TransactionService } from 'src/app/shared/services/transaction.service';
import { User } from 'src/app/shared/models/user';
import { toastProperty } from 'src/app/shared';

@Component({
    selector: 'app-transaction',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
    user: User;
    operators: Array<string> = [];
    vouchers: Array<any> = [];

    constructor(
        public auth: AuthService,
        public transaction: TransactionService,
        public toastService: ToastService,
        public cd: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.user = this.auth.getUser();
        this.transaction.getOperator().toPromise().then((res: any) => {
            if (Number(res.status)) {
                this.operators = res.operator.nama;
            }

        });
    }
    searchVocher(transactionForm: NgForm) {
        console.log(transactionForm);
        transactionForm.controls.voucher.setValue('');
        transactionForm.controls.harga.setValue('');
        this.transaction.getVoucer({operator: transactionForm.value.operator}).toPromise().then((res: any) => {
            if (Number(res.status)) {
                this.vouchers = res.voucher;
            }
        });
    }
    setCurrency(harga: string) {
        return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(Number(harga));
    }
    doTransaction(form: NgForm) {
        if (form.invalid) {
            this.toastService.warning('Please Check Your Form !');
        }
        if (form.valid) {
            this.transaction.doTransaction({userid: this.user.userid, voucher: form.value.voucher.pulsa}).toPromise().then((res: any) => {
                this.toastService[toastProperty[res.status]](res.msg);
                console.log('res', res);
            });

        }
    }
}
