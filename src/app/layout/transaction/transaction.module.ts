import { ErrorMessageModule } from './../../shared/modules/error-messages/error-messages.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionRoutingModule } from './transaction-routing.module';
import { TransactionComponent } from './transaction.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ErrorMessageModule,
        TranslateModule,
        TransactionRoutingModule],
    declarations: [TransactionComponent]
})
export class TransactionModule {}
