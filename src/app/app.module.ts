import { ErrorMessagesComponent } from './../../.history/src/app/error-messages.component_20200131153157';
import { AuthService } from './shared/services/auth.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastService } from './shared/services/toast.service';
import { HttpConfigInterceptor } from './shared/interceptor/httpconfig.interceptor';

import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TransactionService } from './shared/services/transaction.service';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        NgbModule,
        AppRoutingModule,
    ],
    entryComponents: [
    ],
    declarations: [AppComponent],
    providers: [
        ToastService,
        { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
        AuthService,
        TransactionService,
        AuthGuard,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
